# Questões teóricas
 
**O que é Git?.**

Git é uma ferramenta de gerenciamento de códigos. Nela você pode organizar o código do seu software para que fique organizada para todos os integrantes do projeto. No git você tem o código original e ramificações desse código para que se possa ter um histórico completo de alterações feitas durante todo o processo e também intenções de mudança sem mudar o código propriamente.

**O que é a staging area?**

É um ramo onde os integrantes do projeto podem colocar suas intenções de mudança no código sem mudar o código original, assim dando para todos terem acesso a essa possível mudança e para que aconteça os devidos testes antes de mesclar ao projeto main.

**O que é o working directory?**

É um diretório no sistema de arquivos em que você está trabalhando em um projeto Git. É onde você pode ver e editar os arquivos do projeto antes de colocar os mesmos na staging area e fazer o commit.
	
**O que é um commit?**

O commit é quando você salva as alterações que estão na staging area em uma ramificação (branch) específica do seu projeto.

**O que é uma branch?**

É uma ramificação separada do desenvolvimento do projeto Git. Cada branch apresenta uma linha do tempo diferente do seu projeto main, sendo assim possível organizar muito melhor melhorias de código antes de commitar ela para a linha principal.

**O que é o head no Git?**

É uma referência (ponteiro) ao último commit em alguma branch. Ela indica o ponto atual na linha do tempo do projeto na branch.

**O que é um merge?**

É a ação de juntar duas ou mais branches em uma única branch.

**Explique os 4 estados de um arquivo no Git.**

- Untracked - Arquivos que não estão sendo monitorados pelo Git, logo as mudanças nele não serão adicionadas em um futuro commit a menos que ele seja adicionado ao repositório.

- Unmodified - neste estado ele já está sendo monitorado pelo Git mas ainda não teve nenhuma mudança desde o último commit.

- Modified - São arquivos monitorados pelo Git que receberam uma alteração desde o último commit.

- Staged - É um arquivo que foi adicionado a staging area e está pronto para ser incluído no próximo commit.

**Explique o comando git init.**

Comando feito para inicializar um novo repositório Git em um diretório existente.

**Explique o comando git add.**

É utilizado para adicionar arquivos existentes em um repositório.

**Explique o comando git status.**

Utilizamos esse comando para ver quais arquivos foram modificados mas ainda não foram adicionados a staging area ou arquivos que já se encontram na staging area e estão prontos para serem commitados.

**Explique o comando git commit.**

Comando usado para registrar novas mudanças no repositório do projeto em questão. Nesse comando também pode ser feito um comentário para que todos da equipe saibam o que se trata tal commit. Um exemplo seria git commit -m “Essa é uma mensagem de explicação do que se trata o commit feito.”

**Explique o comando git log.**

o Git log é usado para exibir um histórico de commits de um repositório

**Explique o comando git checkout -b.**

Usada para criar uma nova branch e mudar para ela no repositório, ou seja, ela é uma junção de dois comandos, o git branch (criar uma nova branch) e o git checkout (para mudar para o repositório).
- sintaxe:
```
git checkout -b nome_da_branch. 
```

**Explique o comando git reset e suas três opções.**

Utilizado para desfazer alterações no repositório em questão. Podemos usá-la para remover mudanças em arquivos na staging area ou remover commits do histórico de commits.
- sintaxe:
```
git reset opção arquivo

```

onde opções é a opção que deseja desfazer e arquivo é o nome do arquivo que deseja desfazer as alterações.

temos 3 opções:

- soft: mantém as mudanças feitas nos arquivos no working directory e na staging area, mas remove o último commit.

- mixed: mantém mudanças feitas nos arquivos do working directory, mas remove as da staging area, porém podemos adicioná-las novamente e a staging area antes de fazer um novo commit.

- hard: remove mudanças feitas nos arquivos tanto no working directory tanto na staging area. 

**Explique o comando git revert.**

Usado para desfazer alterações feitas em um commit. de um repositório. Ele cria um novo commit que cancela as alterações feitas do commit desejado, permitindo que o histórico de commits seja mantido, permitindo que as alterações sejam revisadas no futuro.

sintaxe: 

temos dois tipos de sintaxe nesse caso. se quisermos reverter as alterações de um commit especifico, usamos o 
```
git revert hash_do_commit
```
 (hash_do_commit é o código que identifica o commit que você deseja)
Podemos também reverter o último commit feito usando o 
```
git revert HEAD 
```
que aponta sempre pro commit mais recente.

**Explique o comando git clone.**

Usado para clonar um repositório existente em outro local. Depois de clonar o repositório, podemos usar o git pull, git push e git commit para trabalhar em alterações localmente e sincronizá-las ao repositório original.

sintaxe: 
```
git clone url_do_repositorio
```

**Explique o comando git push**

O comando git push é usado para enviar alterações locais para o repositório remoto. Feito para que outros desenvolvedores possam colaborar com suas alterações.
sintaxe: 
```
git push Nome_do_repositorio_remoto Nome_da_branch
```

**Explique o comando git pull.**

Usado para baixar as alterações de um repositório remoto e combiná-las com seu repositorio local.

sintaxe:
```
git pull nome_do_repositorio_local Nome_da_branch.
```

**Como ignorar o versionamento de arquivos no Git?**

Basta criar um arquivo .gitignore no diretorio raiz do seu repositorio. Dentro deste arquivo você colocará arquivos que você quer ignorar.

exemplo: se deseja ignorar um arquivo chamado “Token.json” que contém dados sensíveis como um token de acesso, basta adicionar dentro do arquivo .gitignore a seguinte linha de comando. Token.json.

**No terralab utilizamos as branches master ou main, develop e staging. Explique o objetivo de cada uma.**

- Master ou main: É a branch principal do desenvolvimento do projeto.

- develop: É uma branch complementar a branch principal. É nela que armazenamos a última versão estável do código, fazendo com que você possa fazer mudanças nela como correções de bugs ou funcionalidades sem afetar a branch main.

- staging: Complementar a branch develop, é nela que fazemos os testes de modificações feitas pelos desenvolvedores.

# Questões práticas #

**Descubra o que esse código faz através de pesquisas na internet, também descubra como executar um código em javascript e dado que o nome do nosso arquivo é calculadora.js e você entendeu o que o código faz, escreva abaixo como executar esse código em seu terminal:**

Este código faz a soma de dois números passadas como parâmetro na hora de executar o script no terminal.
para fazer a execução no terminal devemos escrever:

```
node calculadora.js "parâmetro_um" "parâmetro_dois"
```

**Que tipo de commit esse código deve ter de acordo ao conventional commit.**
- Usando o conventional commit devemos usar para esse caso o "feat:" pois assim sabemos que foi adicionado uma funcionalidade nova ou uma nova adição ao código, e depois descrevemos essa funconalidade.

**Que tipo de commit o seu README.md deve contar de acordo ao conventional commit.**
- Como se trata de um commit referente a documentação, usamos o "docs:"

**Descubra o que essa mudança representa em relação ao conventional commit e faça o devido commit dessa mudança.**
- Como o código tem a mesma funcionalidade mas essa funcionalide foi introduzida em uma função visando melhorar o código, usamos o conventional commit "refactor:"

**Descubra como executar o seu novo código.**
- Para executar o código precisamos passar como parâmetro dois numeros a serem somados ou subtraídos.
Exemplo:
```
node calculadora.js 10 20 soma
```
que terá como saída o número 30

**Descubra como executar esse novo código e que operações ele é capaz de realizar. Deixe sua resposta aqui, e explique o que essas funções javascript fazem.**
- Para executar o código de João você deve executar o script e passar como parâmetro um numero, uma operação matemática e outro número.
```
node calculadora.js "número_um" "operador_matemático" "número_dois"
```
e um exemplo seria:

```
calculadora.js 10 + 5
```
que teria como saída o numero 15
 as operações possíveis de serem feitas neste script são:
 - "+" para somar
 - "-" para subtrair
 - "*" para multiplicar
 - "/" para dividir
 - "%" para obter o resto da divisão
 - "**" para exponenciação
 - "==" para operador de comparação de igualdade (retorna True se for igual e False caso contrário)
 - "!=" para operador de comparação de diferença (retorna True se for diferente e False caso contrário)
 - ">" para verificar se o primeiro número é maior que o segundo
 - "=>" para verificar se o primeiro número é maior ou igual ao segundo
 - "<" para verificar se o primeiro número é menor que o segundo
 - "<=" para verificar se o primeiro número é menor ou igual ao segundo
